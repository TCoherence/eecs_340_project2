#######################################################################
#         |               |           |                 |
#         | random.com    |           |   random.com    |
#         |==============>| Our AWS   |================>| Google 
# client  |               | DNS Proxy |                 | Public 
#         |<--------------| Server    |<----------------| DNS Server
#         | AWS IP addr   |           |  (....0011)     |
#         |               |           |  No such name   |
#######################################################################


import socket
import signal # use Ctrl-C to stop
import sys    # get argvs
import select # support multi-connection
import binascii
import struct # used to encode int to bytes


MAX_COUNT = 50 # defend fake tcp dns message
# HOST is not hard-coded, I use my AWS as default in case of forgeting type in terminal
HOST = '' # run in the local and access from the outside.
PORT = 53 # should be replaced with 53 later.
TARGET_HOST = '8.8.8.8'
TARGET_PORT = 53
target_server_addr = (TARGET_HOST, TARGET_PORT)
client_addr = ''

# socket establishing
sockUdp = socket.socket(socket.AF_INET, # Internet
                        socket.SOCK_DGRAM) # UDP
sockTcp = socket.socket(socket.AF_INET, #Internet
                        socket.SOCK_STREAM) # TCP
server_addr = (HOST, PORT)
# print >> sys.stderr, 'starting up on %s port %s' % server_add
sockUdp.bind(server_addr)
sockTcp.bind(server_addr)
print >> sys.stderr, 'successfully start udp and tcp port'
sockTcp.listen(5)

inputs = [sockTcp, sockUdp]

# encode from int to bytes
def dns_encode(s):
  # do some work then return, not sure if correct
  return str.join('',map(lambda x: chr(int(x)), s.split('.')))

def int_to_bytes(num):
  return struct.pack(">H", num)

# decode from bytes to int
def dns_decode(s):
  return int(binascii.hexlify(s), 16)


def udp_forward(query):
  clientUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  clientUDP.sendto(query, target_server_addr)
  res, addr = clientUDP.recvfrom(4096)
  print "query = ", repr(query)
  if isTruncated(res) :
    res = manipulate(query)
    res = res[2:]
  print "resUDP = ", repr(res)
  clientUDP.close()
  return res

def isTruncated(res):
  if (len(res)) == 0 :
    return False
  flags = res[2:4] # retrieve flags from Header section
  flags_int = dns_decode(flags)
  if ( flags_int & 0x03 ) == 0x03:
    return True
  else:
    return False

def manipulate(request):
  
  packet = request[0:2]      # header: ID
  packet += b'\x81\x80'      # header: flags
  packet += b'\x00\x01\x00\x01\x00\x00\x00\x00'
  packet += request[12:]
  packet += '\xc0\x0c'         # Pointer to domain name
  packet += '\x00\x01\x00\x01\x00\x00\x00\x3c\x00\x04' # Response type, ttl and resource data length -> 4 bytes
  packet += dns_encode(HOST)
  packet = int_to_bytes(len(packet)) + packet
  print 'manipulated packet = ', repr(packet)
  return packet


def tcp_forward(conn):

  try:
    conn.settimeout(5) # defend "dead connect" attack
    buffer = conn.recv(2) # may cause zero buffer bug.
    if len(buffer) == 0 :
      raise Exception("Buffer is empty!!!...")
    length = dns_decode(buffer)
    print "length = ", length
    # must use length > 0 to make sure no negative value
    count = MAX_COUNT
    while length > 0 : # fix length < 0 scenario
      data = conn.recv(4096) # [WAITING]fix dead waiting scenario
      if len(data) == 0 :
        raise Exception("Buffer is empty!!!...")
      buffer += data
      length -= len(data)
      count = count - 1
      if ( count == 0 ) :
        raise Exception("count reach limit...")
      print "length = ", length
    print "buffer = ", repr(buffer)
    # send to google
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(target_server_addr)
    client.send(buffer)
    # recv from google
    response = client.recv(4096)
    print "response = ", repr(response)

    res = isTruncated(response[2:])
    print "TCP is Truncated = ", res
    if res :
      # do some work to modify the response
      # because previous 2 bytes indicates the length, we skip it.
      response = manipulate(buffer[2:])

    # forward to client
    conn.send(response)
    conn.close()
    inputs.remove(conn)

    client.close()
  except : # fix socket error because sender sends spam message attack.
    # because query leads some error, we ignore it.
    # response = 
    # response = len(response) + response
    # conn.send(response)
    
    conn.close()
    inputs.remove(conn)
  


  

def start():
  try :
    # select 
    while inputs :
      readable, writable, exceptional = select.select(inputs, [], [])
      for s in readable :
        if s is sockUdp:
          query, addr = sockUdp.recvfrom(4096)
          res = udp_forward(query)
          if res :
            sockUdp.sendto(res, addr)
        elif s is sockTcp :
          conn, addr = sockTcp.accept()
          # conn.setblocking(0)
          inputs.append(conn)
        # tcp conn
        else :
          tcp_forward(s)
      print "===finish one packet work==="
  except KeyboardInterrupt:
    print '\nKeyboardInterrupt, turn down the server...'
    sockUdp.close()
    sockTcp.close()

if __name__ == "__main__":
  # deal with input args
  args = sys.argv
  if len(args) > 2:
    print "Wrong arguments number, you should input no more than 1 argument"
    print "Exiting the app..."
    print "" # add one more line to seperate
    sys.exit(1)
  if len(args) == 2:
    HOST = args[1]
    print "HOST = ", HOST
  start()