import socket
import signal # use Ctrl-C to stop
import sys    # get argvs
import select # support multi-connection
import binascii

# UDP doesn't work


MAX_COUNT = 50
HOST = '' # run in the local and access from the outside.
PORT = 53 # should be replaced with 53 later.
TARGET_HOST = '8.8.8.8'
TARGET_PORT = 53
target_server_addr = (TARGET_HOST, TARGET_PORT)
client_addr = ''

# socket establishing
sockUdp = socket.socket(socket.AF_INET, # Internet
                        socket.SOCK_DGRAM) # UDP
sockTcp = socket.socket(socket.AF_INET, #Internet
                        socket.SOCK_STREAM) # TCP
server_addr = (HOST, PORT)
# print >> sys.stderr, 'starting up on %s port %s' % server_add
sockUdp.bind(server_addr)
sockTcp.bind(server_addr)
print >> sys.stderr, 'successfully start udp and tcp port'
sockTcp.listen(5)
sockTcp.setblocking(0)

inputs = [sockTcp, sockUdp]

# def dns_encode(str):
  
def dns_decode(s):
  return int(binascii.hexlify(s), 16)
def udp_forward(query):
  clientUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  clientUDP.sendto(query, target_server_addr)
  res, addr = clientUDP.recvfrom(4096)
  print "query = ", repr(query)
  if isTruncated(res) :
    res = manipulate(query)
    res = res[2:]
  print "resUDP = ", repr(res)
  clientUDP.close()
  return res

# any connection sending packets too long we ignore it.
def tcp_forward(conn):
  try:
    conn.settimeout(5) # defend "dead connect" attack
    buffer = conn.recv(2) # may cause zero buffer bug.
    if len(buffer) == 0 :
      raise Exception("Buffer is empty!!!...")
    length = dns_decode(buffer)
    print "length = ", length
    # must use length > 0 to make sure no negative value
    count = MAX_COUNT
    while length > 0 : # fix length < 0 scenario
      data = conn.recv(4096) # [WAITING]fix dead waiting scenario
      if len(data) == 0 :
        raise Exception("Buffer is empty!!!...")
      buffer += data
      length -= len(data)
      count = count - 1
      if ( count == 0 ) :
        raise Exception("count reach limit...")

      print "length = ", length
    print "buffer = ", repr(buffer)
    # send to google
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(target_server_addr)
    client.send(buffer)
    # recv from google
    response = client.recv(4096)
    print "response = ", repr(response)
    # forward to client
    conn.send(response)
    conn.close()
    inputs.remove(conn)
    client.close()
  except : # fix socket error because sender sends spam message attack.
    # because query leads some error, we ignore it.
    # response = 
    # response = len(response) + response
    # conn.send(response)
    conn.close()
    inputs.remove(conn)

def start():
  try :
    # select 
    while inputs :
      readable, writable, exceptional = select.select(inputs, [], [])
      for s in readable :
        if s is sockUdp:
          query, addr = sockUdp.recvfrom(4096)
          print "queryUDP = ", repr(query)
          res = udp_forward(query)
          if res :
            sockUdp.sendto(res, addr)
        elif s is sockTcp :
          conn, addr = sockTcp.accept()
          # conn.setblocking(0)
          inputs.append(conn)
           
        # tcp conn
        else :
          tcp_forward(conn)
          

  except KeyboardInterrupt:
    print '\nKeyboardInterrupt, turn down the server...'
    sockUdp.close()
    sockTcp.close()

if __name__ == "__main__":
  start()

