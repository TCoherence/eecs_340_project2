import socket
import signal # use Ctrl-C to stop
import sys    # get argvs

HOST = '' # run in the local and access from the outside.
PORT = 53 # should be replaced with 53 later.
TARGET_HOST = '8.8.8.8'
TARGET_PORT = 53
target_server_addr = (TARGET_HOST, TARGET_PORT)
client_addr = ''
# deal with input args

# socket establishing
sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
server_addr = (HOST, PORT)
print >> sys.stderr, 'starting up on %s port %s' % server_addr
sock.bind(server_addr)

def udp_forward(query):
  client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  client.bind((HOST, 10086))
  client.sendto(query, target_server_addr)
  res, addr = client.recvfrom(4096)
  return res

def start():
  try :
    while True :
      #e Listen DNS query on PORT 53
      print >> sys.stderr, '\n1. Waiting to receive message'
      data, addr = sock.recvfrom(4096)
      print >> sys.stderr, '\n2. Received data'
      print 'data = ', repr(data)
      print 'addr = ', addr
      res = udp_forward(data)
      print 'res = ', repr(res)
      if res :
        sock.sendto(res, addr)

      # # if request
      # if ( data[2:4] == '\x01\x00' ) :
      #   # Forward to 8.8.8.8
      #   print 'we come into forward to 8.8.8.8 branch'
      #   client_addr = addr
      #   sent = sock.sendto(data, target_server_addr)
      # else :
      #   # Forward to client
  except KeyboardInterrupt:
    print '\nKeyboardInterrupt, turn down the server...'
    sock.close()

if __name__ == "__main__":
    start()

