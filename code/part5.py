import socket
import signal # use Ctrl-C to stop
import sys    # get argvs
import select # support multi-connection
import Queue
import time

HOST = ''
PORT = 80 # default port number
FILE_PATH = '.' # Because we get /index.html we need set FILE_PATH to Current dir

args = sys.argv
if len(args) > 2:
  print "Wrong arguments number, you should input no more than 1 argument"
  print "Exiting the app..."
  print "" # add one more line to seperate
  sys.exit(1)
if len(args) == 2:
  PORT = int(args[1])

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((HOST, PORT))
print "Now server starts on PORT : ", PORT
server.listen(5)

# =====================================================
# Following several codes are from https://pymotw.com/2/select/
# =====================================================
# Sockets from which we expect to read
inputs = [ server ]
# Sockets to which we expect to write
outputs = [ ]
# Outgoing message queues (socket:Queue)
message_queues = {}

# server.setblocking(0)
try:
    while inputs:
        print "Waiting for the next event..."
        readable, writable, exceptional = select.select(inputs, outputs, inputs)  # don't know why 3rd param is inputs
        # Handle inputs
        for s in readable:
            # if server is idle
            if s is server:
                # A "readable" server socket is ready to accept a connection
                connection, client_address = s.accept()
                print >> sys.stderr, 'new connection from', client_address
                connection.setblocking(0)
                inputs.append(connection)
                # Give the connection a queue for data we want to send
                message_queues[connection] = Queue.Queue()
            else:
                try:
                    data = s.recv(1024)
                
                # ====================================
                # NOW JUST ECHO, NEED TO UPDATED LATER
                # ====================================
                    if data:
                        # A readable client socket has data
                        print >> sys.stderr, 'received "%s" from %s' % (data, s.getpeername())
                        try:
                            if data.split()[0] != "GET":
                                print "Not HTTP GET request, ignore..."
                                continue
                            # # print "data: ", data
                            # file_requested = data.split()[1]  # E.g. /blah/blah/a/a.html
                            # file_path = FILE_PATH + file_requested
                            # print "file_path: ", file_path
                            # tail = file_requested.split('.')[1]

                            # print "tail: ", tail

                            new = data.split('\r\n')
                            for i in range(len(new)):
                                new_part = new[i].split()
                                if new_part[0] == 'Host:':
                                    random = new_part[1]
                                    break
                            if random:
                                # test
                                response = '<!DOCTYPE html>  <html lang="en">  <head>  <meta charset="utf-8">   <title></title>  </head>   <body> <h1>The website: "' + random + '" does not exist, how about trying <a href="https://www.walmart.com/">walmart.com</a></h1>  </body>   </html> '
                            else :
                                raise Exception("not host field detected...")
                            # receive HTTP request
                            # send back file
                            data_res = 'HTTP/1.0 200 OK\r\nConnection: Close\r\nContent-Type: text/html\r\nContent-Length: %d\r\n' % (
                                len(response))
                            data = data_res + '\r\n' + response + '\n'

                        except IOError:
                            response = '<h1>404 Not Found</h1>'
                            data = "HTTP/1.0 404 Not Found\r\nConnection: close\r\nContent-Type: text/html\r\nContent-Length: %d\r\n" % (
                                len(response))
                            data = data + '\r\n' + response + '\n'
                        except IndexError:
                            response = '<h1>403 Forbidden</h1>'
                            data = "HTTP/1.0 403 Forbidden\r\nConnection: close\r\nContent-Type: text/html\r\nContent-Length: %d\r\n" % (
                                len(response))
                            data = data + '\r\n' + response + '\n'

                        # close connect
                        print "enqueue the message..."
                        message_queues[s].put(data)

                        # Add output channel for response
                        if s not in outputs:
                            print "append s to outputs..."
                            outputs.append(s)
                    else:
                        # Interpret empty result as closed connection
                        print >> sys.stderr, 'closing', client_address, 'after reading no data'
                        # Stop listening for input on the connection
                        if s in outputs:
                            outputs.remove(s)
                        inputs.remove(s)
                        s.close()

                        # Remove message queue
                        del message_queues[s]
                except:
                    print "maybe reset by peer, del this conn..."
                    if s in outputs:
                        outputs.remove(s)
                    inputs.remove(s)
                    s.close()
                    # Remove message queue
                    del message_queues[s]  
                    
        # Handle outputs
        for s in writable:
            try:
                next_msg = message_queues[s].get_nowait()
            except Queue.Empty:
                # No messages waiting so stop checking for writability.
                print >> sys.stderr, 'output queue for', s.getpeername(), 'is empty'
                outputs.remove(s)
            else:
                print >> sys.stderr, 'sending "%s" to %s' % (next_msg, s.getpeername())
                s.send(next_msg)
        # Handle "exceptional conditions"
        for s in exceptional:
            print >> sys.stderr, 'handling exceptional condition for', s.getpeername()
            # Stop listening for input on the connection
            inputs.remove(s)
            if s in outputs:
                outputs.remove(s)
            s.close()

            # Remove message queue
            del message_queues[s]

    print "Turn down the server..."
    server.close()
except KeyboardInterrupt:
    print "\nkeyboardInterrupt, turn down the server..."
    server.close()
